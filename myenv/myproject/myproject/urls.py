from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from website import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index),
    url(r'^index', views.index),
    url(r'^questions', views.contact),
    url(r'^contacts', views.contacts),
    url(r'^about', views.about),
    url(r'^success_donation', views.success_donation),
    url(r'^cancelled_donation', views.cancelled_donation),
)

urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))

urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)
