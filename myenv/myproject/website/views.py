from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template import loader, RequestContext
from .forms import ContactForm
from .models import Contact
from datetime import datetime

def index(request):
    template = loader.get_template('index.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

def about(request):
    template = loader.get_template('aboutInactive.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

def contacts(request):
    # template = loader.get_template('contacts.html')
    # context = RequestContext(request, {})
    # return HttpResponse(template.render(context))

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Contact.objects.create(name=data['name'],
                                   email = data['email'],
                                   subject = data['subject'],
                                   message = data['message'],
                                   date = datetime.now(),
                                   ready = False
                                   )
            message = """
                You receive this message:
                %s

                FROM: %s
            """ %(data['message'], data['email'])
            send_mail(data['subject'], message, "simopopov@gmail.com", ["popov@pfiffigsoft.com"])
            return render_to_response('thanks.html',
                RequestContext(request, {
                    'ok': True,
                    'name': data['name'],
                    'email': data['email']
                    }))
        else:
            form = ContactForm(request.POST)
            return render_to_response('contacts.html',
                RequestContext(request, {
                    'ok': False,
                    'form': form,
                    'error': "Result did not match"}))
    else:
        form = ContactForm()
        return render_to_response('contacts.html',
            RequestContext(request, {
                'form': form,
                }))

def contact(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Contact.objects.create(name=data['name'],
                                   email = data['email'],
                                   subject = data['subject'],
                                   message = data['message'],
                                   date = datetime.now(),
                                   ready = False
                                   )
            message = """
                You receive this message:
                %s

                FROM: %s
            """ %(data['message'], data['email'])
            send_mail(data['subject'], message, "simopopov@gmail.com", ["popov@pfiffigsoft.com"])
            return render_to_response('thanks.html',
                RequestContext(request, {
                    'ok': True,
                    'name': data['name'],
                    'email': data['email']
                    }))
        else:
            form = ContactForm(request.POST)
            return render_to_response('questions.html',
                RequestContext(request, {
                    'ok': False,
                    'form': form,
                    'error': "Result did not match"}))
    else:
        form = ContactForm()
        return render_to_response('questions.html',
            RequestContext(request, {
                'form': form,
                }))


def success_donation(request):
    return render_to_response('success_donation.html',
        RequestContext(request, {}))


def cancelled_donation(request):
    return render_to_response('cancelled_donation.html',
        RequestContext(request, {}))

