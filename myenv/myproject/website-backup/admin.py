from django.contrib import admin
from .models import Contact
# Register your models here.

class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'subject', 'date', 'ready')
    search_fields = ('name', 'email', 'subject', 'ready')
    ordering = ('ready', 'date')
    list_filter = ('ready',)

admin.site.register(Contact, ContactAdmin)