#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'simeon'



from django import forms
from simplemathcaptcha.fields import MathCaptchaField
class ContactForm(forms.Form):
    name = forms.CharField(label=u'Name:')
    email = forms.EmailField(label=u'Email:')
    subject = forms.CharField(label=u'Subject:')
    message = forms.CharField(widget=forms.Textarea, label=u'Message:')
    captcha = MathCaptchaField(label=u'Bots protection')